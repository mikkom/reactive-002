/**
 * Copyright (C) 2009-2013 Typesafe Inc. <http://www.typesafe.com>
 */
package actorbintree

import akka.actor._
import scala.collection.immutable.Queue

object BinaryTreeSet {

  trait Operation {
    def requester: ActorRef
    def id: Int
    def elem: Int
  }

  trait OperationReply {
    def id: Int
  }

  /** Request with identifier `id` to insert an element `elem` into the tree.
    * The actor at reference `requester` should be notified when this operation
    * is completed.
    */
  case class Insert(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request with identifier `id` to check whether an element `elem` is present
    * in the tree. The actor at reference `requester` should be notified when
    * this operation is completed.
    */
  case class Contains(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request with identifier `id` to remove the element `elem` from the tree.
    * The actor at reference `requester` should be notified when this operation
    * is completed.
    */
  case class Remove(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request to perform garbage collection*/
  case object GC

  /** Holds the answer to the Contains request with identifier `id`.
    * `result` is true if and only if the element is present in the tree.
    */
  case class ContainsResult(id: Int, result: Boolean) extends OperationReply
  
  /** Message to signal successful completion of an insert or remove operation. */
  case class OperationFinished(id: Int) extends OperationReply

}


class BinaryTreeSet extends Actor {
  import BinaryTreeSet._
  import BinaryTreeNode._

  def createRoot: ActorRef = context.actorOf(BinaryTreeNode.props(0, initiallyRemoved = true))

  var root = createRoot

  // optional
  var pendingQueue = Queue.empty[Operation]

  // optional
  def receive = normal

  // optional
  /** Accepts `Operation` and `GC` messages. */
  val normal: Receive = {
    case op: Operation => root ! op
    case msg @ GC =>
      val newRoot = createRoot
      root ! CopyTo(newRoot)
      context become garbageCollecting(newRoot)
  }

  // optional
  /** Handles messages while garbage collection is performed.
    * `newRoot` is the root of the new binary tree where we want to copy
    * all non-removed elements into.
    */
  def garbageCollecting(newRoot: ActorRef): Receive = {
    case op: Operation => pendingQueue = pendingQueue.enqueue(op)
    case CopyFinished =>
      root ! PoisonPill
      root = newRoot
      while (pendingQueue.nonEmpty) {
        val (msg, queue) = pendingQueue.dequeue
        root ! msg
        pendingQueue = queue
      }
      context become normal
  }
}

object BinaryTreeNode {
  trait Position

  case object Left extends Position
  case object Right extends Position

  case class CopyTo(treeNode: ActorRef)
  case object CopyFinished

  def props(elem: Int, initiallyRemoved: Boolean) = Props(classOf[BinaryTreeNode], elem, initiallyRemoved)
}

class BinaryTreeNode(val elem: Int, initiallyRemoved: Boolean) extends Actor {
  import BinaryTreeNode._
  import BinaryTreeSet._

  var subtrees = Map[Position, ActorRef]()
  var removed = initiallyRemoved

  // optional
  def receive = normal

  def getElem(op: Operation) = op match {
    case Insert(_, _, e) => e
    case Contains(_, _, e) => e
    case Remove(_, _, e) => e
  }

  // optional
  /** Handles `Operation` messages and `CopyTo` requests. */
  val normal: Receive = {
    case op: Operation =>
      val msgElem = getElem(op)
      if (elem == msgElem) {
        op match {
          case Insert(requester, id, _) =>
            removed = false
            requester ! OperationFinished(id)
          case Contains(requester, id, _) =>
            requester ! ContainsResult(id, !removed)
          case Remove(requester, id, _) =>
            removed = true
            requester ! OperationFinished(id)
        }
      } else {
        val position = if (msgElem < elem) Left else Right
        subtrees get position match {
          case Some(child) => child ! op
          case None =>
            op match {
              case Insert(requester, id, _) =>
                subtrees =
                  subtrees.updated(position, context.actorOf(BinaryTreeNode.props(msgElem, initiallyRemoved = false)))
                requester ! OperationFinished(id)
              case Contains(requester, id, _) =>
                requester ! ContainsResult(id, result = false)
              case Remove(requester, id, _) =>
                requester ! OperationFinished(id)
            }
        }
      }
    case CopyTo(newRoot) =>
      if (!removed) {
        newRoot ! Insert(self, 0, elem)
      }
      val children = subtrees.values
      children.foreach(_ ! CopyTo(newRoot))
      context become copying(children.toSet, insertConfirmed = false)
      finishIfNothingPending(children.toSet, insertConfirmed = false)
  }

  def finishIfNothingPending(expected: Set[ActorRef], insertConfirmed: Boolean) = {
    if ((removed || insertConfirmed) && expected.isEmpty) {
      context.parent ! CopyFinished
    }
  }

  // optional
  /** `expected` is the set of ActorRefs whose replies we are waiting for,
    * `insertConfirmed` tracks whether the copy of this node to the new tree has been confirmed.
    */
  def copying(expected: Set[ActorRef], insertConfirmed: Boolean): Receive = {
    case OperationFinished(_) =>
      finishIfNothingPending(expected, insertConfirmed = true)
      context become copying(expected, insertConfirmed = true)
    case CopyFinished =>
      val remainingExpected = expected - sender
      finishIfNothingPending(remainingExpected, insertConfirmed)
      context become copying(remainingExpected, insertConfirmed)
  }
}
