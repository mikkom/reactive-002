package kvstore

import akka.actor.{Cancellable, Props, Actor, ActorRef}
import scala.concurrent.duration._
import scala.language.postfixOps

object Replicator {
  case class Replicate(key: String, valueOption: Option[String], id: Long)
  case class Replicated(key: String, id: Long)
  
  case class Snapshot(key: String, valueOption: Option[String], seq: Long)
  case class SnapshotAck(key: String, seq: Long)

  def props(replica: ActorRef): Props = Props(new Replicator(replica))
}

class Replicator(val replica: ActorRef) extends Actor {
  import Replicator._
  import Replica._
  import context.dispatcher

  /*
   * The contents of this actor is just a suggestion, you can implement it in any way you like.
   */

  // map from sequence number to pair of sender and request
  var acks = Map.empty[Long, (ActorRef, Replicate)]
  // a sequence of not-yet-sent snapshots (you can disregard this if not implementing batching)
  var pending = Vector.empty[Snapshot]

  var cancellables = Map.empty[Long, Cancellable]

  var _seqCounter = 0L
  def nextSeq = {
    val ret = _seqCounter
    _seqCounter += 1
    ret
  }

  
  /* TODO Behavior for the Replicator. */
  def receive: Receive = {
    case msg @ Replicate(key, valueOption, id) =>
      val seq = nextSeq
      acks = acks.updated(seq, (sender(), msg))
      val cancellable =
        context.system.scheduler.schedule(0 millis, 100 millis, replica, Snapshot(key, valueOption, seq))
      cancellables = cancellables.updated(seq, cancellable)
    case SnapshotAck(key, seq) => acks get seq match {
      case Some((replicateMsgSender, Replicate(_, _, id))) =>
        // Cancel retries
        // FIXME: Refactor this nested pattern matching
        cancellables get seq match {
          case Some(cancellable) =>
            cancellable.cancel()
            cancellables = cancellables - seq
          case None => throw new Exception("Missing cancellable for sequence number " + seq)
        }
        replicateMsgSender ! Replicated(key, id)
      case None => throw new Exception("Missing element in acks map, sequence number: " + seq)
    }
  }

}
