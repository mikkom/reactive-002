package kvstore

import akka.actor._
import kvstore.Arbiter._
import scala.collection.immutable.Queue
import akka.actor.SupervisorStrategy.Restart
import scala.annotation.tailrec
import akka.pattern.{ ask, pipe }
import scala.concurrent.duration._
import akka.util.Timeout
import scala.language.postfixOps

object Replica {
  sealed trait Operation {
    def key: String
    def id: Long
  }
  case class Insert(key: String, value: String, id: Long) extends Operation
  case class Remove(key: String, id: Long) extends Operation
  case class Get(key: String, id: Long) extends Operation

  sealed trait OperationReply
  case class OperationAck(id: Long) extends OperationReply
  case class OperationFailed(id: Long) extends OperationReply
  case class GetResult(key: String, valueOption: Option[String], id: Long) extends OperationReply

  def props(arbiter: ActorRef, persistenceProps: Props): Props = Props(new Replica(arbiter, persistenceProps))
}

class Replica(val arbiter: ActorRef, persistenceProps: Props) extends Actor {
  import Replica._
  import Replicator._
  import Persistence._
  import context.dispatcher

  /*
   * The contents of this actor is just a suggestion, you can implement it in any way you like.
   */

  override val supervisorStrategy = SupervisorStrategy.stoppingStrategy

  var kv = Map.empty[String, String]
  // a map from secondary replicas to replicators
  var secondaries = Map.empty[ActorRef, ActorRef]
  // the current set of replicators
  var replicators = Set.empty[ActorRef]

  var persistence = context.watch(context.actorOf(persistenceProps))

  var pendingOperations = Map.empty[Long, (Cancellable, ActorRef, Cancellable, Set[ActorRef])]

  override def preStart(): Unit = {
    arbiter ! Join
  }

  def receive = {
    case JoinedPrimary   => context.become(leader)
    case JoinedSecondary => context.become(replica(0L))
  }

  /* TODO Behavior for  the leader role. */
  val leader: Receive = {
    case Insert(key, value, id) =>
      kv = kv.updated(key, value)
      val cancellable =
        context.system.scheduler.schedule(0 millis, 100 millis, persistence, Persist(key, Some(value), id))
      val cancellable2 =
        context.system.scheduler.scheduleOnce(1 seconds, sender(), OperationFailed(id))
      pendingOperations = pendingOperations.updated(id, (cancellable, sender(), cancellable2, replicators))
      replicators.foreach(_ ! Replicate(key, Some(value), id))
    case Remove(key, id) =>
      kv = kv.-(key)
      val cancellable =
        context.system.scheduler.schedule(0 millis, 100 millis, persistence, Persist(key, None, id))
      val cancellable2 =
        context.system.scheduler.scheduleOnce(1 seconds, sender(), OperationFailed(id))
      pendingOperations = pendingOperations.updated(id, (cancellable, sender(), cancellable2, replicators))
      replicators.foreach(_ ! Replicate(key, None, id))
    case Persisted(key, id) =>
      pendingOperations.get(id) match {
        case Some((cancellable, opSender, cancellable2, pendingReplications)) =>
          cancellable.cancel()
          pendingOperations = pendingOperations.updated(id, (null, opSender, cancellable2, pendingReplications))
          if (pendingReplications.isEmpty) {
            cancellable2.cancel()
            opSender ! OperationAck(id)
            pendingOperations = pendingOperations - id
          }
        case None => if (id != -1L) throw new Exception("Cancellable not found for id " + id)
      }
    case Terminated(actor) =>
      persistence = context.watch(context.actorOf(persistenceProps))
    case Replicated(key, id) =>
      pendingOperations.get(id) match {
        case Some((cancellable, opSender, cancellable2, pendingReplications)) =>
          pendingOperations =
            pendingOperations.updated(id, (cancellable, opSender, cancellable2, pendingReplications - sender()))
          if (cancellable == null && (pendingReplications - sender()).isEmpty) {
            cancellable2.cancel()
            opSender ! OperationAck(id)
            pendingOperations = pendingOperations - id
          }
        case None => if (id != -1L) throw new Exception("Cancellable not found for id " + id)
      }
    case Get(key, id) =>
      sender ! GetResult(key, kv.get(key), id)
    case Replicas(replicaNodes) =>
      val newNodes = (replicaNodes - self) -- secondaries.keys
      newNodes foreach {
        case node =>
          // Add new node
          val replicator = context.actorOf(Replicator.props(node))
          secondaries = secondaries.updated(node, replicator)
          kv.foreach {
            case (key: String, value: String) =>
              replicator ! Replicate(key, Some(value), -1L) // FIXME: What id to use here?
            // FIXME: Handle "Replicated" replies somewhere, maybe
          }
      }

      secondaries.foreach {
        case (node, replicator) =>
          if (replicaNodes contains node) {
            // Continue using this node
          } else {
            // Remove this node
            pendingOperations = pendingOperations.map {
              case (id, (cancellable, opSender, cancellable2, pendingReplications)) =>
                // Waive pending replications to the removed replicator
                val newPendingReplications = pendingReplications - replicator
                if (cancellable == null && newPendingReplications.isEmpty) {
                  cancellable2.cancel()
                  opSender ! OperationAck(id)
                  // FIXME: Leaving dummy marker, we should get rid of these
                  (-666L, (null, null, null, null))
                } else {
                  (id, (cancellable, opSender, cancellable2, newPendingReplications))
                }
            }

            context.stop(replicator)
            secondaries = secondaries - node
          }
      }

      replicators = secondaries.values.toSet
  }

  /* TODO Behavior for the replica role. */
  def replica(lastSeq: Long): Receive = {
    case Snapshot(key, valueOption, seq) =>
      if (seq < lastSeq) {
        sender ! SnapshotAck(key, seq)
      } else if (seq == lastSeq) {
        kv = valueOption match {
          case Some(value) => kv.updated(key, value)
          case None => kv - key
        }
        val cancellable =
          context.system.scheduler.schedule(0 millis, 100 millis, persistence, Persist(key, valueOption, seq))
        pendingOperations = pendingOperations.updated(seq, (cancellable, sender(), null, null))
        context.become(replica(lastSeq + 1))
      } else {
        // Just ignore the overly high sequence
      }
    case Get(key, id) =>
      sender ! GetResult(key, kv.get(key), id)
    case Persisted(key, id) =>
      pendingOperations.get(id) match {
        case Some((cancellable, replicator, _, _)) =>
          cancellable.cancel()
          pendingOperations = pendingOperations - id
          replicator ! SnapshotAck(key, id)
        case None => throw new Exception("Cancellable not found for id " + id)
      }
    case Terminated(actor) =>
      persistence = context.watch(context.actorOf(persistenceProps))
  }
}
