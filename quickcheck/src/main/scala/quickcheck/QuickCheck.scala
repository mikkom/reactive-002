package quickcheck

import common._

import scala.math.min
import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

  def areEqual(h1: H, h2: H): Boolean = {
    if (isEmpty(h1) && isEmpty(h2)) {
      true
    } else {
      findMin(h1) == findMin(h2) && areEqual(deleteMin(h1), deleteMin(h2))
    }
  }

  def satisfiesHeapProperty(h: H): Boolean = {
    def loop(h: H, min: Int): Boolean = {
      val h2 = deleteMin(h)
      isEmpty(h2) || {
        val min2 = findMin(h2)
        min2 >= min && loop(h2, min2)
      }
    }
    isEmpty(h) || loop(h, findMin(h))
  }

  lazy val genHeap: Gen[H] = for {
    a <- arbitrary[Int]
    h <- oneOf(const(empty), genHeap)
  } yield insert(a, h)

  lazy val genMap: Gen[Map[Int,Int]] = for {
    k <- arbitrary[Int]
    v <- arbitrary[Int]
    m <- oneOf(const(Map.empty[Int,Int]), genMap)
  } yield m.updated(k, v)

  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)

  property("testSingleElementHeap") = forAll { a: Int =>
    val h = insert(a, empty)
    findMin(h) == a
  }

  property("insertToEmptyAndDelete") = forAll { a: Int =>
    val h = deleteMin(insert(a, empty))
    isEmpty(h)
  }

  property("testInsert") = forAll { (h: H) =>
    val m = if (isEmpty(h)) 0 else findMin(h)
    val h2 = insert(m, h)
    findMin(h2) == m && satisfiesHeapProperty(h2)
  }

  property("testOrdering") = forAll { (h: H) =>
    satisfiesHeapProperty(h)
  }

  property("testInsertingOrMeldingNewMinimum") = forAll { (a: Int, h: H) =>
    val h2 = insert(a, h)
    val min = findMin(h2)
    // Guard for overflow
    min - 1 >= min || {
      val newMinHeap = insert(min - 1, h2)
      val singletonHeap = insert(min - 1, empty)
      val newMinHeap2 = meld(singletonHeap, h2)
      findMin(newMinHeap) == min - 1 &&
        areEqual(deleteMin(newMinHeap), h2) &&
        areEqual(newMinHeap, newMinHeap2)
    }
  }

  property("testMelding") = forAll { (h1: H, h2: H) =>
    if (isEmpty(h1) && isEmpty(h2)) {
      isEmpty(meld(h1, h2))
    } else if (isEmpty(h1)) {
      areEqual(h2, meld(h1, h2))
    } else if (isEmpty(h2)) {
      areEqual(h1, meld(h1, h2))
    } else {
      val m1 = findMin(h1)
      val m2 = findMin(h2)
      val h = meld(h1, h2)
      findMin(h) == min(m1, m2) && satisfiesHeapProperty(h)
    }
  }

  property("testMeldingAssociativity") = forAll { (h1: H, h2: H) =>
    areEqual(meld(h1, h2), meld(h2, h1))
  }
}
