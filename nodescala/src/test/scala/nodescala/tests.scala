package nodescala

import sun.reflect.generics.reflectiveObjects.NotImplementedException

import scala.language.postfixOps
import scala.util.{Try, Success, Failure}
import scala.collection._
import scala.concurrent._
import ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.async.Async.{async, await}
import org.scalatest._
import NodeScala._
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class NodeScalaSuite extends FunSuite {

  test("A Future should always be completed") {
    val always = Future.always(517)

    assert(Await.result(always, 0 nanos) == 517)
  }

  test("A Future should never be completed") {
    val never = Future.never[Int]

    try {
      Await.result(never, 1 second)
      assert(false)
    } catch {
      case t: TimeoutException => // ok!
    }
  }

  test("Test a list of futures") {
    val fs = Future.all(List(Future.always(517), Future.always(256)))

    assert(Await.result(fs, 1 second) == List(517, 256))
  }

  test("Test a list of epic fail futures") {
    val fs = Future.all(List(Future.always(517), Future.failed(new NotImplementedException)))
    try {
      Await.result(fs, 1 second)
      assert(false)
    } catch {
      case _: TimeoutException => assert(false)
      case _: Throwable => // ok!
    }
  }

  test("Test delay") {
    val f1 = Future.delay(1 second)
    try {
      Await.result(f1, 800 milliseconds)
      assert(false)
    } catch {
      case _: TimeoutException => // ok!
      case _: Throwable => assert(false)
    }

    val f2 = Future.delay(1 second)
    try {
      Await.result(f2, 1200 milliseconds)
    } catch {
      case _: Throwable => assert(false)
    }
  }

  test("Test any") {
    val fs = Future.any(List(Future.delay(500 milliseconds), Future.failed(new Exception("boom"))))
    try {
      Await.result(fs, 1 second)
      assert(false)
    } catch {
      case _: TimeoutException => assert(false)
      case _: Throwable => // ok!
    }
  }

  test("Test any again") {
    val fs = Future.any(List(Future.delay(500 milliseconds), Future.delay(200 milliseconds), Future.always(512)))
    assert(Await.result(fs, 1 second) == 512)
  }

  test("Test now") {
    assert(Future.always(513).now == 513)

    try {
      Future.delay(1 second).now
      assert(false)
    } catch {
      case _: NoSuchElementException => // ok!
      case _: Throwable => assert(false)
    }

    try {
      Future.failed(new Exception("boomstick says boom!")).now
      assert(false)
    } catch {
      case _: NoSuchElementException => assert(false)
      case _: Throwable => // ok!
    }
  }

  test("CancellationTokenSource should allow stopping the computation") {
    val cts = CancellationTokenSource()
    val ct = cts.cancellationToken
    val p = Promise[String]()

    async {
      while (ct.nonCancelled) {
        // do work
      }

      p.success("done")
    }

    cts.unsubscribe()
    assert(Await.result(p.future, 1 second) == "done")
  }

  test("CancellationTokenSource should allow stopping the computation, improved") {
    val p = Promise[String]()

    val cts = Future.run() { ct =>
      async {
        while (ct.nonCancelled) {
          // do work
        }

        p.success("done")
      }
    }
    cts.unsubscribe()
    assert(Await.result(p.future, 1 second) == "done")
  }

  test("continueWith should wait for the first future to complete") {
    val delay = Future.delay(1 second)
    val always = (f: Future[Unit]) => 42

    try {
      Await.result(delay.continueWith(always), 500 millis)
      assert(false)
    }
    catch {
      case t: TimeoutException => // ok
    }
  }

  class DummyExchange(val request: Request) extends Exchange {
    @volatile var response = ""
    val loaded = Promise[String]()
    def write(s: String) {
      response += s
    }
    def close() {
      loaded.success(response)
    }
  }

  class DummyListener(val port: Int, val relativePath: String) extends NodeScala.Listener {
    self =>

    @volatile private var started = false
    var handler: Exchange => Unit = null

    def createContext(h: Exchange => Unit) = this.synchronized {
      assert(started, "is server started?")
      handler = h
    }

    def removeContext() = this.synchronized {
      assert(started, "is server started?")
      handler = null
    }

    def start() = self.synchronized {
      started = true
      new Subscription {
        def unsubscribe() = self.synchronized {
          started = false
        }
      }
    }

    def emit(req: Request) = {
      val exchange = new DummyExchange(req)
      if (handler != null) handler(exchange)
      exchange
    }
  }

  class DummyServer(val port: Int) extends NodeScala {
    self =>
    val listeners = mutable.Map[String, DummyListener]()

    def createListener(relativePath: String) = {
      val l = new DummyListener(port, relativePath)
      listeners(relativePath) = l
      l
    }

    def emit(relativePath: String, req: Request) = this.synchronized {
      val l = listeners(relativePath)
      l.emit(req)
    }
  }
  test("Server should serve requests") {
    val dummy = new DummyServer(8191)
    val dummySubscription = dummy.start("/testDir") {
      request => for (kv <- request.iterator) yield (kv + "\n").toString
    }

    // wait until server is really installed
    Thread.sleep(500)

    def test(req: Request) {
      val webpage = dummy.emit("/testDir", req)
      val content = Await.result(webpage.loaded.future, 1 second)
      val expected = (for (kv <- req.iterator) yield (kv + "\n").toString).mkString
      assert(content == expected, s"'$content' vs. '$expected'")
    }

    test(immutable.Map("StrangeRequest" -> List("Does it work?")))
    test(immutable.Map("StrangeRequest" -> List("It works!")))
    test(immutable.Map("WorksForThree" -> List("Always works. Trust me.")))

    dummySubscription.unsubscribe()
  }

}




