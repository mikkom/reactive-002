package calculator

import org.scalacheck.Gen
import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

import org.scalatest._
import org.scalacheck.Prop._

import TweetLength.MaxTweetLength
import org.scalatest.prop.Checkers

@RunWith(classOf[JUnitRunner])
class CalculatorSuite extends FunSuite with ShouldMatchers with Checkers {

  /******************
   ** TWEET LENGTH **
   ******************/

  def tweetLength(text: String): Int =
    text.codePointCount(0, text.length)

  test("tweetRemainingCharsCount with a constant signal") {
    val result = TweetLength.tweetRemainingCharsCount(Var("hello world"))
    assert(result() == MaxTweetLength - tweetLength("hello world"))

    val tooLong = "foo" * 200
    val result2 = TweetLength.tweetRemainingCharsCount(Var(tooLong))
    assert(result2() == MaxTweetLength - tweetLength(tooLong))
  }

  test("tweetRemainingCharsCount with a supplementary char") {
    val result = TweetLength.tweetRemainingCharsCount(Var("foo blabla \uD83D\uDCA9 bar"))
    assert(result() == MaxTweetLength - tweetLength("foo blabla \uD83D\uDCA9 bar"))
  }


  test("colorForRemainingCharsCount with a constant signal") {
    val resultGreen1 = TweetLength.colorForRemainingCharsCount(Var(52))
    assert(resultGreen1() == "green")
    val resultGreen2 = TweetLength.colorForRemainingCharsCount(Var(15))
    assert(resultGreen2() == "green")

    val resultOrange1 = TweetLength.colorForRemainingCharsCount(Var(12))
    assert(resultOrange1() == "orange")
    val resultOrange2 = TweetLength.colorForRemainingCharsCount(Var(0))
    assert(resultOrange2() == "orange")

    val resultRed1 = TweetLength.colorForRemainingCharsCount(Var(-1))
    assert(resultRed1() == "red")
    val resultRed2 = TweetLength.colorForRemainingCharsCount(Var(-5))
    assert(resultRed2() == "red")
  }

  def satisfiesVietesTheorem(aVal: Double, bVal: Double, cVal: Double): Boolean = {
    val a = Var(aVal)
    val b = Var(bVal)
    val c = Var(cVal)
    val delta = Polynomial.computeDelta(a, b, c)
    val solutions = Polynomial.computeSolutions(a, b, c, delta)
    if (delta() >= 0) {
      equals(solutions().sum, -b() / a(), 1e-10) &&
        equals(solutions().product, c() / a(), 1e-10)
    } else {
      solutions().isEmpty
    }
  }

  test("testReactivity") {
    val a = Var(7.5)
    val b = Var(3.7)
    val c = Var(-3.2)
    val delta = Polynomial.computeDelta(a, b, c)
    val solutions = Polynomial.computeSolutions(a, b, c, delta)
    if (delta() >= 0) {
      assertEquals(solutions().sum, -b() / a(), 1e-6)
      assertEquals(solutions().product, c() / a(), 1e-6)
    } else {
      assert(solutions().isEmpty)
    }

    a.update(5.5)
    b.update(2.4)
    c.update(-99.1)
    if (delta() >= 0) {
      assertEquals(solutions().sum, -b() / a(), 1e-6)
      assertEquals(solutions().product, c() / a(), 1e-6)
    } else {
      assert(solutions().isEmpty)
    }
  }

  def equals(a: Double, b: Double, delta: Double): Boolean = {
    math.abs(a - b) < delta
  }

  def assertEquals(a: Double, b: Double, delta: Double) = {
    assert(equals(a, b, delta))
  }

  val reasonableDouble = Gen.choose(-1e100, 1e100)

  val propVietesTheorem = forAll(reasonableDouble, reasonableDouble, reasonableDouble) {
    (a: Double, b: Double, c: Double) => satisfiesVietesTheorem(a, b, c)
  }

  test("vietesTheoremHolds") {
    check(propVietesTheorem)
  }
}
