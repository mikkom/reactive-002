package calculator

object Polynomial {
  def computeDelta(a: Signal[Double], b: Signal[Double],
      c: Signal[Double]): Signal[Double] = {
    Signal {
      val bVal = b()
      bVal * bVal - 4 * a() * c()
    }
  }

  def computeSolutions(a: Signal[Double], b: Signal[Double],
      c: Signal[Double], delta: Signal[Double]): Signal[Set[Double]] = {
    Signal {
      val deltaVal = delta()
      if (deltaVal < 0) {
        Set.empty
      } else {
        def root(op: (Double, Double) => Double) = {
          op(-b(), scala.math.sqrt(deltaVal)) / (2 * a())
        }
        Set(root(_+_), root(_-_))
      }
    }
  }
}
